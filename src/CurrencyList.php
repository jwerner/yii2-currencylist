<?php
namespace diggindata\currencylist;

use Yii;
use yii\base\BaseObject;

class CurrencyList extends BaseObject
{
    /**
     * @var mixed The list of currencies.
     */
    private static $_list;

    /**
     * @var string The Git package name of the original list
     */ 
    private static $_packageName = 'umpirsky/currency-list';

    public function __construct($config = [])
    {
        // ... initialization before configuration is applied

        parent::__construct($config);
    }

    /**
     * Returns a list of translated currencyy names, indexed by 3-char currency code
     * @return mixed
     */
    public function init()
    {
        parent::init();
        if(!is_dir(Yii::getAlias('@vendor/'.self::$_packageName.'/data')))
            throw new \yii\web\HttpException(500, 'Missing package for currency-list');
            
        // ... initialization after configuration is applied
    }   

    public static function getList()
    {
        // Get current app language, transformed for lists i18n subdirs:
        $language = str_replace('-', '_', Yii::$app->language);

        if(is_array(self::$_list))
            return self::$_list;

        $dir = Yii::getAlias('@vendor/'.self::$_packageName.'/data/'.$language);
        if(is_dir($dir)) {
            self::$_list = require($dir.'/currency.php');
        } else {
            if(strlen($language)==5) {
                $language = substr($language, 0, 2);
                self::$_list = require(Yii::getAlias('@vendor/'.self::$_packageName.'/data/'.$language.'/currency.php'));
            } else {
                self::$_list = require(Yii::getAlias('@vendor/'.self::$_packageName.'/data/en/currency.php'));
            }
        }
        return self::$_list;
    } 

}
