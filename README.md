<p align="center">
    <a href="https://bitbucket.org/jwerner/" target="_blank">
        <img src="https://www.diggin-data.de/dd-cms/files/Logo_Diggin_Data.jpg" height="100px">
    </a>
    <h1 align="center">Currency List Extension for Yii 2</h1>
    <br>
</p>

This package is based on [umpirsky/currency-list](https://github.com/umpirsky/currency-list) 

It contains an extension for the Yii framework to get a translated list of currency names.

[![Latest Stable Version](https://poser.pugx.org/diggindata/yii2-currencylist/v/stable)](https://packagist.org/packages/diggindata/yii2-currencylist)
[![Total Downloads](https://poser.pugx.org/diggindata/yii2-currencylist/downloads)](https://packagist.org/packages/diggindata/yii2-currencylist)

Installation 
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist diggindata/yii2-currencylist "*"
```

or add

```
"diggindata/yii2-currencylist": "*"
```

to the require section of your composer.json.

Then run `composer install` or `composer update`.

Usage
-----

The extension looks at the configured Yii application language `Yii::$app->language`. So if the language is configured to e.g. `de`, it takes the corresponding translation list from the umpirsky package.

Once the extension is installed, simply use the list in your forms as follows:

    use diggindata\currencylist\CurrencyList;    
    <?= $form->field($model, 'currencyCode')->dropDownList(CurrencyList::getList()) ?>

Alternatively, you may add the extension as an application component.

Add the following to your `config/web.php` file:

    ...
    'components' => [
        'currencyList' => [
            'class' => 'diggindata\currencylist\CurrencyList',
        ],

You can the use it like this:

    <?= $form->field($model, 'currencyCode')->dropDownList(Yii::$app->currenyList->getList()) ?>


